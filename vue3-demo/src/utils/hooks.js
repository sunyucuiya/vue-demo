import { useStore, mapState, mapGetters } from "vuex";
import { computed } from 'vue';

export function useState(mapArr) {
 const store = useStore();
 const arrFn = mapState(mapArr);
 const storeState = {};
 Object.keys(arrFn).forEach(key => {
  const fn = arrFn[key].bind({$store: store}); //组件外部使用$store指向的也是vuex store
  storeState[key] = computed(fn)
 });
 return storeState;
}
export function useGetter(mapArr) {
  const store = useStore();
  const arrFn = mapGetters(mapArr);
  const storeGetter = {};
  Object.keys(arrFn).forEach(key => {
    const fn = arrFn[key].bind({ $store: store });
    storeGetter[key] = computed(fn)
  })
  return storeGetter;
}