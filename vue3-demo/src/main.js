import './assets/main.css'

import { createApp } from 'vue'

import App from './App.vue'
import router from './router'
import store from './store'


// 创建实例app，绑定根组件
const app = createApp(App)
// 应用插件 router, store
app.use(router)
app.use(store)
// 配置 应用实例会暴露一个 .config 对象允许我们配置一些应用级的选项
app.config.errorHandler = (err, vm, info) => {
  console.log(err, vm, info)
}
// 真实dom挂在到实例， 应用实例必须在调用了 .mount() 方法后才会渲染出来
// #app 是一个实际的 DOM 元素或是一个 CSS 选择器字符串
app.mount('#app')
