/**
 * @description vuex demo
 * npm install vuex@next --save
 * store: {state, mutations, action, getter module}
    * 创建 store = createStore({state, mutations, actions, getters modules})
    * 注册 app.use(store)
    * 组件中使用：
      * this.$store
      * setup(){const store = useStore(); }
      * 解构map辅助函数
 * state: 
    * 单一状态树-只有一个store实例，一般用返回函数的形式 防止污染
    * 响应式--组件中作为计算属性调用 this.$store.state.namex
    * mapState辅助函数 
      * mapState(["count"]),
      * mapState({count: state => state.count, c: 'count', comp(state){return ...}}) 
      * 可以...展开
    * 要根据需求分析什么变量适合放到store中用vuex来管理
 * getter:
    * store 的计算属性
    * 接受 state 作为其第一个参数，接受其他 getter 作为第二个参数
    * mapGetters(["func1", "func2"])
 * mutations:
    * 同步事件
    * 用commit来触发 store.commit({type: 'increment',amount: 10}) type是指向事件名，其余参数是payload
    * ...mapMutations({add: 'increment'}) 将 `this.add()` 映射为 `this.$store.commit('increment')`
 * actions:
    * 提交mutation的异步版本，参数是与store相同的cxt对象,在action中commit mutation
    * 通过store.dispatch方法触发 可以链式promise  store.dispatch({type: 'increment',amount: 10})
    * mapActions
 * modules:
    * store 可以分割成多个模块
    * rootState为根结点状态， action和getter（第三参）可以获取
    * 命名空间
 */
import { createStore } from 'vuex'
export default createStore({
  state() {
    return {
      count: 1,
      name: 'sunyucui',
      age: 26
    }
  },
  getters: {
    double(state) {
      return state.count * 2
    }
  },
  mutations: {
    increment(state) {
      state.count++
    },
    changeName(state,name){
      state.name = name;
    },
    changeAge(state, age){
      state.age = age;
    }

  },
  actions: {
    increment(context) {
      setTimeout(() => {
        context.commit('increment')
      }, 2000)
    }
  }
})