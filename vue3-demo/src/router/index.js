import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('@/views/AboutView.vue')
    },
    {
      path: '/vuex-demo',
      name: 'vuex-demo',
      component: () => import('@/views/VuexView.vue')
    },
    {
      path: '/vuex3-demo',
      name: 'vuex3-demo',
      component: () => import('@/views/setupView/SetupView.vue')
    },
    {
      path: '/render-api',
      name: 'render-api',
      component: () => import('@/views/renderApi/index.vue')
    },
  ]
})

export default router
