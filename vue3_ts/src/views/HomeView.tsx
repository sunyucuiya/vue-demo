import { defineComponent, provide, ref } from "vue";
import type { InjectionKey, ComponentPublicInstance } from "vue";
import MyComponent from "@/components/MyComponent";
import {message} from '@/utils/key'



export default defineComponent({
  setup(props, { emit }) {
      provide(message, 'this is a message')
      // const childRef = ref<ComponentPublicInstance|null>(null)
      const childRef = ref<InstanceType<typeof MyComponent>|null>(null)

      const handleClick = () => {
        console.log('父组件调用子组件方法,childRef.value',childRef.value)
        childRef.value?.handleChildClick()
      }

      return () => (
        <>
          <MyComponent ref={childRef}/>
          <button onClick={handleClick}>父组件按钮，focus子组件</button>
        </>
      );
  },
})