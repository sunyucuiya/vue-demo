import type { InjectionKey } from "vue";

export const message = Symbol() as InjectionKey<string>