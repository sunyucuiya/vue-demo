import { computed, defineComponent, reactive, ref, provide, inject} from "vue";
import type { PropType, Ref, InjectionKey} from "vue";
import {message} from '@/utils/key'
// 定义接口类型
interface Book {
  id: number
  title: string
  author: string
  price: number
  tags: string[]
  isPublished?: boolean
} 

interface ExposedAPI {
  handleChildClick: () => void
}

export default defineComponent({
  props: {
    books: {
      type: Object as PropType<Book[]>
    }
  },
  setup(props, cxt) {
    const { emit, expose } = cxt
    // console.log('cxt',cxt) // attrs, emit, expose, slot
    //ref
    const year: Ref<string | number> = ref(2000) // 指定类型
    const month = ref<string | number>(1) // 指定范型
    const n = ref<number>(1) //推导得到的类型：Ref<number | undefined>
    const input = ref()
    //reactive
    const book: Book = reactive({
      id: 1,
      title: 'Hello World',
      author: 'Jack',
      price: 100,
      tags: ['tag1', 'tag2']
    })
    // inject
    const foo = inject(message)
    //computed 通过泛型参数显式指定类型
    const double = computed<Number>(()=>n.value * 2)
    // 使用类型断言
    const onChange = (event: Event) => {
      // emit('change') // <-- 类型检查 / 自动补全
      const target = (event.target as HTMLInputElement).value

    }
    const handleChildClick = ():void => {
      console.log('子组件方法','handleChildClick')
      input.value.focus()
    }
    // 方法暴露给父组件
    expose({
      handleChildClick 
    } as ExposedAPI)

    return () => (
      <div>
        <input ref={input} value='a text'/>
        {foo}
      </div>
    )
  }
})