// import WorkerThree from "@/assets/js/three.worker.js"
self.addEventListener('message', (e) => {
  //console.log 浏览器可能会不支持
  // console.log('Title-worker:[two-worker]get msg from main', e.data)
  console.log(e)
  self.postMessage('two get msg from main'+e.data)

  const port2 = e.ports[0];
  port2.postMessage('two said to one using port2')
  port2.onmessage = (e) => {
    console.log('two get msg :', e.data)
  }
}, false)
// self.close() //关闭自身
/**
 * self.importScripts加载js脚本不支持相对路径，要用第三方http
 * 只有部分浏览器支持 worker内部创建worker
 */
// self.importScripts("three.worker.js")

self.onerror = function (e) {
  console.log('worker error', e)
}