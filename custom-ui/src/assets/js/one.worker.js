self.addEventListener('message', (e) => {
  console.log(e)
  const port1 = e.ports[0]
  self.postMessage('one get msg from main'+e.data)

  port1.postMessage('one said to two using port1')
  port1.onmessage = (e) => {
    console.log('one get msg :', e.data)
  }
}, false)
// self.close() //关闭自身
self.onerror = (e) => {
  console.log('Title-worker:[one-worker]error', e)
}