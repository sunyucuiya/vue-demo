import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LeftBar from '@/components/LeftBar'
import RightBar from '@/components/RightBar'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    components: {
      default: HomeView,
      LeftBar,
      RightBar
    },
  },
  {
    path: '/about',
    name: 'about',
    alias: '/a',
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView'),
    children: [
      {
        path: 'userList/:id',
        name: 'userList',
        // props: true,
        props: route => ({
          id: route.params.id,
          name: route.query.name
        }),
        component: () => import('@/components/UserList.vue'),
      },
    ],
    //路由独享守卫,没有被调用？？
    beforeEach: (to, from, next) => {
      console.log('路由独享守卫 to and from', to, from);
      next();
    }
  },
  {
    path: '/audio',
    name: 'audio',
    component: () => import('@/views/AudioView.vue')
  },
  {
    path: '/front-base',
    name: 'front-base',
    component: () => import('@/views/FrontBase.vue')
  },
  { //重定向用法
    path: '/testPath',
    // redirect:'/about'
    // redirect: {
    //   name:'about'
    // }
    redirect: to => {
      console.log('to', to) //route 对象
      return {
        name: 'about',
        to
      }
    } 
  }
  /**
   * 动态路由配置
   */
  // {
  //   path: '/userList/:id',
  //   name: 'userList',
  //   component: () => import('@/components/UserList.vue'),
  // },
];

const router = new VueRouter({
  mode: 'history',
  // mode: 'hash',
  base: process.env.BASE_URL,
  routes
})
/**
 *    全局路由守卫 全局前置守卫
 */
// router.beforeEach((to, from, next) => {
//   console.log('全局前置守卫! to and from', to, from)
//   next();
// })
/**
 * 全局后置钩子
 */
// router.afterEach((to, from) => {
//   console.log('全局后置钩子! to and from',to,from)
// })
export default router
